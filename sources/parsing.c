//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

static int parse_res(char **tab, t_scene *rt)
{
	rt->w = ft_atoi(tab[1]);
	rt->h = ft_atoi(tab[2]);
	if (rt->w < 0 || rt->h < 0)
	{
		error_message("Screen resolution wrong argument.\n");
		return (0);
	}
	return (1);
}

static int check_id(char **tab, t_scene *rt)
{
	if (!ft_strcmp(tab[0], "R"))
		return (parse_res(tab, rt));
	else if (!ft_strcmp(tab[0], "sp"))
		return (parse_sphere(tab, rt));
	else if (!ft_strcmp(tab[0], "L"))
		return (parse_light(tab, rt));
	else if (!ft_strcmp(tab[0], "A"))
		return (parse_ambient(tab, rt));
	else if (!ft_strcmp(tab[0], "cy"))
		return (parse_cylinder(tab, rt));
	else if (!ft_strcmp(tab[0], "pl"))
		return (parse_plane(tab, rt));
    else if (!ft_strcmp(tab[0], "C"))
	{
		return (parse_camera(tab, rt));
	}
	else
	{
		error_message("Wrong identifier.\n");
		return (0);
	}
	return (1);
}

static int		split_tab_and_free(char **tab, t_scene *rt, char *line)
{
	tab = ft_split(line, ' ');
	if (tab[0])
	{
		if (!check_id(tab, rt))
			return (0);
	}
	free_tab(tab);
	free(line);
	line = NULL;
	return (1);
}

int 	parse(char *file, t_scene *rt)
{
	int fd;
	char *line;
	char **tab;

	tab = NULL;
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		error_message("Can't open file.\n");
		return (0);
	}
	while ((get_next_line(fd, &line)) > 0)
	{
		if (!split_tab_and_free(tab, rt, line))
			return (0);
	}
	//if (!split_tab_and_free(tab, rt, line))
	//	return (0);
	close(fd);
	return (1);
}

//
// Created by alangloi on 12/9/21.
//

#include "minirt.h"

int parse_light(char **tab, t_scene *rt)
{
	char 	**crd;
	char	**rgb;

	rt->l.intensity = ft_atoi_double(tab[2]);
	if (rt->l.intensity < 0.0000 || rt->l.intensity > 1.0000)
	{
		error_message("Light cant be negative either superior to 1.\n");
		return (0);
	}
	crd = ft_split(tab[1], ',');
	rgb = ft_split(tab[3], ',');
	rt->l.pos.x = ft_atoi_double(crd[0]);
	rt->l.pos.y = ft_atoi_double(crd[1]);
	rt->l.pos.z = ft_atoi_double(crd[2]);
	rt->l.col.b = ft_atoi_double(rgb[0]);
	rt->l.col.g = ft_atoi_double(rgb[1]);
	rt->l.col.r = ft_atoi_double(rgb[2]);
	if (rt->l.col.b < 0 || rt->l.col.b > 255 || rt->l.col.g < 0 || rt->l.col.g > 255
		|| rt->l.col.r < 0 || rt->l.col.r > 255)
	{
		free_tab(crd);
		free_tab(rgb);
		error_message("Wrong arguments color light.\n");
		return (0);
	}
    free_tab(crd);
	free_tab(rgb);
	return (1);
}

int parse_ambient(char **tab, t_scene *rt)
{
	char 	**rgb;

	rgb = ft_split(tab[2], ',');
	rt->a.intensity = ft_atoi_double(tab[1]);
	if (rt->a.intensity < 0.0000 || rt->a.intensity > 1.0000)
	{
		error_message("Light cant be negative either superior to 1.\n");
		return (0);
	}
	rt->a.col.b = ft_atoi_double(rgb[0]);
	rt->a.col.g = ft_atoi_double(rgb[1]);
	rt->a.col.r = ft_atoi_double(rgb[2]);
	if (rt->a.col.b < 0 || rt->a.col.b > 255 || rt->a.col.g < 0 || rt->a.col.g > 255
		|| rt->a.col.r < 0 || rt->a.col.r > 255)
	{
		free_tab(rgb);
		error_message("Wrong arguments color ambient.\n");
		return (0);
	}
	free_tab(rgb);
	return (1);
}
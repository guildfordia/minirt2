//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

void adding_lum(t_color *col, double lum, t_color light_col)
{
	col->r = lum * col->r * (light_col.r / 255);
	col->g = lum * col->g * (light_col.g / 255);
	col->b = lum * col->b * (light_col.b / 255);
}

void adding_light(t_color *col, double new, t_color light_col)
{
	t_color add;

	add = *col;
	adding_lum(&add, new, light_col);
	col->r = col->r + add.r;
	col->g = col->g + add.g;
	col->b = col->b + add.b;
}

int     shadow_intersect(t_ray *l_ray, t_scene *rt)
{
    t_object *cur;
    double max;

    cur = rt->obj;
    max = norm_vector(l_ray->dir);
    while (cur)
    {
        intersect_object(l_ray, cur);
        if (l_ray->i.t1 > 0.000001 && l_ray->i.t1 < max)
            return (1);
        if (l_ray->i.t2 > 0.000001 && l_ray->i.t2 < max)
            return (1);
        cur = cur->next;
    }
    return (0);
}

void		get_light(t_ray *r, t_scene *rt)
{
	t_vector	dist;
	//t_color		s_col;
	//t_color		ambi;
	//t_color		scene;
    //t_color     diff;
	double 		inter;
	double 		power;
	t_ray		light_r;
    //t_color     light;

	(void)rt;
	(void)light_r;
	//printf("%f\n", l->pos.x);
	//printf("%f\n", l->pos.y);
	//printf("%f\n\n", l->pos.z);
	//printf("%f\n\n", l->intensity);
	//printf("%f\n", l->col.r);
	//printf("%f\n", l->col.g);
	//printf("%f\n\n\n", l->col.b);
	//printf("%f\n\n", a->intensity);
	//printf("%f\n", a->col.r);
	//printf("%f\n", a->col.g);
	//printf("%f\n\n\n", a->col.b);

	//for (t_light *x = cur; x; x = x->next)
	//	printf("%f %f %f\n", x->pos.x, x->pos.y, x->pos.z);
	dist = sub_vector(rt->l.pos, r->point);
	//printf("dist %f %f %f\n", dist.x, dist.y, dist.z);
	//printf("normal %f %f %f\n", r->normal.x, r->normal.y, r->normal.z);
	inter = dot_vector(r->normal, dist);
	//printf("inter %f\n", inter);
	power = rt->l.intensity * inter / (norm_vector(r->normal) * norm_vector(dist));
	//printf("int %f\n", rt->l.intensity);
	//printf("power %f\n", power);
	//light_r.ori = r->point;
	//light_r.dir = dist;
	//s_col = vec_to_col(mult_vector_prod(power, col_to_vec(s->col)));
	//light = mult_color_prod(power, l->col);
	//diff = add_color(light, s_col);
	//printf("1 %f\n", r->pixel.r);
	//printf("1 %f\n", r->pixel.g);
	//printf("1 %f\n\n", r->pixel.b);
    light_r.ori = r->point;
    light_r.dir = dist;
    if (!shadow_intersect(&light_r, rt))
    {
        if (power > 0 && dot_vector(r->dir, r->normal) < 0)
            adding_light(&r->pixel, power, rt->l.col);
    }
	//printf("2 %f\n", r->pixel.r);
	//printf("2 %f\n", r->pixel.g);
	//printf("2 %f\n\n", r->pixel.b);
	//printf("3 %f\n", r->pixel.r);
	//printf("3 %f\n", r->pixel.g);
	//printf("3 %f\n\n\n\n", r->pixel.b);
	//ambi = mult_color_prod(a->intensity, a->col);
	//scene = add_color(ambi, diff);
}

//
// Created by Antoine LANGLOIS on 15/09/2021.
//

#include "minirt.h"

void 			normalized(t_vector *v)
{
	*v = normalize(*v);
}

double rotate_degres(double rot)
{
    return (rot * 180);
}

double rad(double n)
{
    n = n * M_PI / 180;
    return (n);
}

/*
void 			del_spaces(char *line)
{

}
 */
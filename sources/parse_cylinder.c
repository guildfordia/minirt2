//
// Created by alangloi on 12/8/21.
//

#include "minirt.h"

int parse_cylinder(char **tab, t_scene *rt)
{
	t_cylinder *new;

	new = alloc_cylinder(tab);
	if (!new)
	{
		error_message("Wrong argument when allocating cylinder.\n");
		return (0);
	}
	if (!add_object((t_cylinder *)new, &rt->obj, CY))
	{
		error_message("Can't add cylinder to scene.\n");
		return (0);
	}
	return (1);
}

t_cylinder *alloc_cylinder(char **tab)
{
	t_cylinder *c;
	char	**crd;
	char 	**rgb;
	char 	**dir;

	c = malloc(sizeof(t_cylinder));
	if (!c)
		return (NULL);
	init_vector(&c->dir, 0, 1, 0);
	c->d = ft_atoi_double(tab[3]);
	if (c->d < 0)
		return (NULL);
	c->h = ft_atoi_double(tab[4]);
	if (c->h < 0)
		return (NULL);
	crd = ft_split(tab[1], ',');
	dir = ft_split(tab[2], ',');
	rgb = ft_split(tab[5], ',');
	c->pos.x = ft_atoi_double(crd[0]);
	c->pos.y = ft_atoi_double(crd[1]);
	c->pos.z = ft_atoi_double(crd[2]);
	c->rot.x = ft_atoi_double(dir[0]);
	c->rot.y = ft_atoi_double(dir[1]);
	c->rot.z = ft_atoi_double(dir[2]);
	c->col.b = ft_atoi_double(rgb[0]);
	c->col.g = ft_atoi_double(rgb[1]);
	c->col.r = ft_atoi_double(rgb[2]);
	if (c->col.b < 0 || c->col.b > 255 || c->col.g < 0 || c->col.g > 255
		|| c->col.r < 0 || c->col.r > 255 || c->rot.x < -1.0000
		|| c->rot.x > 1.0000 || c->rot.y < -1.0000 || c->rot.y > 1.0000
		|| c->rot.z < -1.0000 || c->rot.z > 1.0000)
	{
		free_tab(crd);
		free_tab(rgb);
		free_tab(dir);
		return (NULL);
	}
	free_tab(crd);
	free_tab(dir);
	free_tab(rgb);
	calculate_rotate(&c->dir, c->rot);
	c->pos2 = add_vector(c->pos, mult_vector_prod(c->h, c->dir));
	return (c);
}
//
// Created by Antoine LANGLOIS on 9/16/21.
//

#include "minirt.h"

t_color		mult_color(t_color c1, t_color c2)
{
	t_color	result;

	result.r = c1.r * c2.r;
	result.g = c1.g * c2.g;
	result.b = c1.b * c2.b;
	return (result);
}

t_color		mult_color_prod(double c, t_color col)
{
	t_color	result;

	result.r = col.r * c;
	result.g = col.g * c;
	result.b = col.b * c;
	return (result);
}

t_color		dot_color(t_color col)
{
	t_color	result;

	result.r = col.r * col.r;
	result.g = col.g * col.g;
	result.b = col.b * col.b;
	return (result);
}

t_color		sqrt_color(t_color col)
{
	t_color	result;

	result.r = sqrt(col.r);
	result.g = sqrt(col.g);
	result.b = sqrt(col.b);
	return (result);
}
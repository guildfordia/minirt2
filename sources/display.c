//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

void print_screen(t_scene *rt, t_color *pixel)
{
	int i;

	i = (rt->x * rt->bpp / 8) + ((rt->h - 1 - rt->y) * rt->size_line);
	rt->img_data[i + 0] = minmax(pixel->r);
	rt->img_data[i + 1] = minmax(pixel->g);
	rt->img_data[i + 2] = minmax(pixel->b);
}

void viewport_init(t_scene *rt)
{
    double ratio;

    ratio = rt->w / rt->h;
    rt->vp_d = 1;
    rt->vp_h = tan((rt->cam.fov * M_PI / 100) / 2) * rt->vp_d * 2;
    rt->vp_w = ratio * rt->vp_h;
}

void display(t_scene *rt)
{
	t_ray		r;

    viewport_init(rt);
	rt->y = 0;
	while (rt->y < rt->h)
	{
		rt->x = 0;
		while (rt->x < rt->w)
		{
            init_vector(&r.dir, 0, 0, 0);
            r.ori = rt->cam.pos;
            r.dir = add_vector(r.dir, rt->cam.dir_z);
            r.dir = add_vector(r.dir, mult_vector_prod(viewport_x(center_x(rt->x, rt), rt), rt->cam.dir_x));
            r.dir = add_vector(r.dir, mult_vector_prod(viewport_y(center_y(rt->y, rt), rt), rt->cam.dir_y));
			r.pixel = init_color(0, 0, 0);
            intersection(&r, rt);
			print_screen(rt, &r.pixel);
			rt->x++;
		}
		rt->y++;
	}
}

//
// Created by user42 on 12/12/2021.
//

#include "minirt.h"

int     center_x(int x, t_scene *rt)
{
    x = -(rt->w / 2) + x;
    return (x);
}

int     center_y(int y, t_scene *rt)
{
    y = (rt->h / 2) - y;
    return (y);
}

double  viewport_x(int x, t_scene *rt)
{
    double num;

    num = (double)x * rt->vp_w / rt->w;
    return (num);
}

double  viewport_y(int y, t_scene *rt)
{
    double num;

    num = (double)y * rt->vp_h / rt->h;
    return (num);
}


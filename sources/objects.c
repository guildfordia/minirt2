//
// Created by alangloi on 12/9/21.
//

#include "minirt.h"

int	count_object_list(t_scene *rt)
{
    t_object *cur;

    rt->cur = 0;
    rt->max = 0;
    cur = rt->obj;
    while (cur)
    {
        rt->max++;
        cur = cur->next;
    }
    return (1);
}

int 	add_object(void *obj, t_object **list, int id)
{
	t_object *cur;
	t_object *new;

	new = malloc(sizeof(t_object));
	if (!new)
		return (0);
	new->next = NULL;
	new->id = id;
	new->obj = obj;
	if (!*list)
		*list = new;
	else
	{
		cur = *list;
		while (cur->next != NULL)
			cur = cur->next;
		cur->next = new;
	}
	return (1);
}

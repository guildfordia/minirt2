#include "minirt.h"

static int	launch_minirt(t_scene *rt)
{
	rt->win_ptr = mlx_new_window(rt->mlx_ptr, rt->w, rt->h, "MiniRT");
	mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img_ptr, 0, 0);
	mlx_key_hook(rt->win_ptr, key_hook, rt);
	mlx_hook(rt->win_ptr, 17, 0, close_window, rt);
	mlx_loop(rt->mlx_ptr);
	return (0);
}

static int	initialize_mlx(t_scene *rt)
{
	rt->mlx_ptr = mlx_init();
	rt->img_ptr = mlx_new_image(rt->mlx_ptr, rt->w, rt->h);
	rt->img_data = (unsigned char *)mlx_get_data_addr(rt->img_ptr, &rt->bpp, &rt->size_line, &rt->endian);
	rt->head = &rt->obj;
	return (0);
}

int	main(int argc , char **argv)
{
	t_scene	rt;

	if (argc != 2)
		return (error_message("Invalid number of arguments\n"));
	if (ft_is_rt(argv[1]) == 0)
		return (error_message("File is not .rt file\n"));
	rt.obj = NULL;
	if (!parse(argv[1], &rt))
	{
		free_obj(&rt);
		return (0);
	}
	initialize_mlx(&rt);
	count_object_list(&rt);
	print_help_1();
    	print_help_2();
	display(&rt);
	launch_minirt(&rt);
	return (0);
}

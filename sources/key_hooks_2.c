//
// Created by user42 on 14/12/2021.
//

#include "minirt.h"

static void obj_key_modify_2(t_scene *rt, int keycode)
{
    double      i;
    double      j;
    int         count;
    t_object    *tmp;

    i = 10;
    j = 0.1;
    count = 0;
    tmp = rt->obj;
    while (count < rt->cur)
    {
        count++;
        tmp = tmp->next;
    }
    sphere_modify(tmp, i, j, keycode);
    cylinder_modify(tmp, i, j, keycode);
    plane_modify(tmp, i, j, keycode);
    if (keycode == NEXT_OBJ) {
        if (rt->cur == rt->max - 1)
            rt->cur = 0;
        else
            rt->cur++;
    }
}

int 	obj_key_modify(int keycode, t_scene *rt)
{
    if ((keycode == OBJ_FRONT) || (keycode == OBJ_LEFT) || (keycode == OBJ_BACK)
        || (keycode == OBJ_RIGHT) || (keycode == SIZE_P) || (keycode == SIZE_M)
        || (keycode == OBJ_ROT_L) || (keycode == OBJ_ROT_R) || (keycode == OBJ_ROT_D)
        || (keycode == OBJ_ROT_U) || (keycode == LENGTH_M)
        || (keycode == LENGTH_P) || (keycode == NEXT_OBJ))
    {
        obj_key_modify_2(rt, keycode);
        display(rt);
        mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img_ptr, 0, 0);
    }
    return (0);
}
//
// Created by user42 on 14/12/2021.
//

#include "minirt.h"

void sphere_modify(t_object *tmp, double i, double j, int keycode)
{
    t_sphere *sph;

    (void)j;
    if (tmp->id == SP)
    {
        sph = (t_sphere *)tmp->obj;
        if (keycode == OBJ_FRONT)
            sph->pos.z += i;
        if (keycode == OBJ_LEFT)
            sph->pos.x -= i;
        if (keycode == OBJ_BACK)
            sph->pos.z -= i;
        if (keycode == OBJ_RIGHT)
            sph->pos.x += i;
        if (keycode == SIZE_P)
            sph->rad += i;
        if (keycode == SIZE_M)
            sph->rad -= i;
    }
}

void cylinder_moves(t_cylinder *cyl, double i, double j, int keycode)
{
    if (keycode == OBJ_ROT_L)
        cyl->rot.y -= j;
    if (keycode == OBJ_ROT_R)
        cyl->rot.y += j;
    if (keycode == OBJ_ROT_D)
        cyl->rot.x -= j;
    if (keycode == OBJ_ROT_U)
        cyl->rot.x += j;
    if (keycode == OBJ_FRONT)
        cyl->pos.z += i;
    if (keycode == OBJ_LEFT)
        cyl->pos.x -= i;
    if (keycode == OBJ_BACK)
        cyl->pos.z -= i;
    if (keycode == OBJ_RIGHT)
        cyl->pos.x += i;
}

void cylinder_modify(t_object *tmp, double i, double j, int keycode)
{
    t_cylinder *cyl;

    if (tmp->id == CY)
    {
        cyl = (t_cylinder *)tmp->obj;
        cylinder_moves(cyl, i, j, keycode);
        if (keycode == SIZE_P)
            cyl->d += i;
        if (keycode == SIZE_M)
            cyl->d -= i;
        if (keycode == LENGTH_P)
            cyl->h += i;
        if (keycode == LENGTH_M)
            cyl->h -= i;
        init_vector(&cyl->dir, 0, 1, 0);
        calculate_rotate(&cyl->dir, cyl->rot);
        cyl->pos2 = add_vector(cyl->pos, mult_vector_prod(cyl->h, cyl->dir));
    }
}

void plane_moves(t_plane *pla, double i, double j, int keycode)
{
    if (keycode == OBJ_ROT_L)
        pla->dir.y -= j;
    if (keycode == OBJ_ROT_R)
        pla->dir.y += j;
    if (keycode == OBJ_ROT_D)
        pla->dir.x -= j;
    if (keycode == OBJ_ROT_U)
        pla->dir.x += j;
    if (keycode == OBJ_FRONT)
        pla->pos.z += i;
    if (keycode == OBJ_LEFT)
        pla->pos.x -= i;
    if (keycode == OBJ_BACK)
        pla->pos.z -= i;
    if (keycode == OBJ_RIGHT)
        pla->pos.x += i;
}

void plane_modify(t_object *tmp, double i, double j, int keycode)
{
    t_plane		*pla;

    if (tmp->id == PL)
    {
        pla = (t_plane *)tmp->obj;
        plane_moves(pla, i, j, keycode);
        init_vector(&pla->normal, 0, 1, 0);
        calculate_rotate(&pla->normal, pla->dir);
    }
}
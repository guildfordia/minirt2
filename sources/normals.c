//
// Created by alangloi on 12/8/21.
//

#include "minirt.h"

t_vector	normal_sphere(t_ray *r, t_sphere *s)
{
	t_vector	n;

	printf("pouit\n");
	n = sub_vector(r->point, s->pos);
	return (n);
}

t_vector	normal_cylinder(t_ray *r, t_cylinder *c)
{
	t_vector	n;

	printf("pouut\n");
	n = sub_vector(c->pos, r->point);
	n = normalize(cross_vector(n, c->dir));
	n = cross_vector(n, c->dir);
	return (n);
}

t_vector	normal_plane(t_ray *r, t_plane *p)
{
	t_vector	n;

	(void)r;
	printf("pouat\n");
	n = p->normal;
	return (n);
}

t_vector	normal_obj(t_ray *r)
{
	t_vector	n;
	t_object	*elem;

	printf("pouet\n");
	elem = r->closest_obj;
	printf("pouet %d\n", elem->id);
	init_vector(&n, 0, 0, 0);
	printf("pouet\n");
	if (elem->id == SP)
	{
		printf("tota\n");
		n = normal_sphere(r, (t_sphere *)elem->obj);
	}
	if (elem->id == CY)
	{
		printf("toto\n");
		n = normal_cylinder(r, (t_cylinder *)elem->obj);
	}
	if (elem->id == PL)
	{
		printf("toti\n");
		n = normal_plane(r, (t_plane *)elem->obj);
	}
	if (dot_vector(r->dir, n) > 0)
	{
		printf("totu\n");
		n = mult_vector_prod(-1, n);
	}
	printf("tote\n");
	n = normalize(n);
	printf("toty\n");
	return(n);
}
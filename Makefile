UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Linux)
	SYSTEM = linux/
	IMPORT_MLX = -lXext -lX11 -lm -lbsd
else
	SYSTEM = macos/
	IMPORT_MLX = -Lmlx -lmlx -framework OpenGL -framework AppKit
endif

NAME = minirt
SOURCES = gcc
FLAGS = -Wall -Werror -Wextra
MLX = mlx/libmlx.a
HEADER = includes
SRC = sources
OBJ = objects
SOURCES = main.c \
		parsing.c \
		error_handler.c \
		display.c \
		intersection.c \
		lights.c \
		colors.c \
		normals.c \
		objects.c \
		rotation.c \
		print_help.c \
		parse_cylinder.c \
		object_intersection.c \
		parse_sphere.c \
		parse_camera.c \
		parse_plane.c \
		parse_light.c \
		get_next_line.c \
		viewport.c \
		split.c \
		atoi_double.c \
		vector_op_1.c \
		vector_op_2.c \
		color_op_1.c \
        color_op_2.c \
        free.c \
		utils_1.c \
		utils_2.c \
		utils_3.c \
		key_hooks_1.c \
		key_hooks_2.c \
		key_hooks_3.c \
		key_hooks_4.c
SRCS = $(addprefix $(SRC)/, $(SOURCES))
OBJS = $(addprefix $(OBJ)/, $(SOURCES:.c=.o))

all: $(OBJ) $(NAME)

$(OBJ):
	mkdir -p $(OBJ)

$(NAME): $(OBJS)
	make -C $(SYSTEM)mlx
	$(CC) $(FLAGS) -o $(NAME) $(OBJS) $(SYSTEM)$(MLX) $(IMPORT_MLX)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(FLAGS) -o $@ -c $^ -I$(HEADER) -Imlx -O3 -g

clean:
	make clean -C $(SYSTEM)mlx
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all fclean clean re
